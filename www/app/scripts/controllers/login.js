angular.module('wwwApp')
    .controller('LoginCtrl', function ($scope, $http, $location, notificationService) {
        $scope.login = function () {
            $http.get("/agencies/getAgency/" + $scope.email)
                .success(function (result) {
                    if (result.success) {
                        if ($scope.password === result.data.password) {
                            $http.get("/agencies/loginAgency/" + result.data.agencyId)
                                .success(function (result) {
                                    if (result.success === true) {
                                        $location.url("/dashboard");
                                    } else {
                                        notificationService.error('Something appears to have gone wrong. Please refresh the' +
                                            'page and try again.');
                                    }
                                })
                        } else {
                            notificationService.error('Invalid username or password. Please try again.');
                        }
                    } else {
                        notificationService.error('Invalid username or password. Please try again.');
                    }
                })
                .error(function (err) {
                    console.log(err);
                })
        }
    });