'use strict';

/**
 * @ngdoc function
 * @name wwwApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the wwwApp
 */
angular.module('wwwApp')
    .controller('DashboardCtrl', function ($scope, $http, $routeParams, $sce, $location) {

        function onload() {
            $scope.myPromiseAgency = $http.get("/agencies/getAuthAgency/")
                .success(function (result) {
                    if (result.success === true) {
                        $scope.agency = result.agency;
                        $scope.myPromiseNewsletter = $http.get('/agencies/getnewsletter/' + $scope.agency.agencyId + '/estate/buy')
                            .success(function (result) {
                                if (result.data != '"fail"') {
                                    $scope.newsletterBuy = result.data;
                                    $scope.changeNewsletter("buy");
                                }
                            });
                        $http.get('/agencies/getnewsletter/' + $scope.agency.agencyId + '/estate/rent')
                           .success(function (result) {
                               if (result.data != '"fail"') {
                                   $scope.newsletterRent = result.data;
                               }
                           });
                        $http.get('/agencies/getnewsletter/' + $scope.agency.agencyId + '/commercial/for-sale')
                         .success(function (result) {
                             if (result.data != '"fail"') {
                                 $scope.newsletterComSale = result.data;
                                 if (!$scope.newsletterBuy) {
                                     $scope.changeNewsletter("for-sale");
                                 }
                             }
                         });
                        $http.get('/agencies/getnewsletter/' + $scope.agency.agencyId + '/commercial/for-lease')
                         .success(function (result) {
                             if (result.data != '"fail"') {
                                 $scope.newsletterComLease = result.data;                                
                             }
                         });
                    } else {
                        $location.url("/login");
                    }
                });
        }

        $scope.changeNewsletter = function (type) {
            if (type === "buy") {
                $scope.newsletter = $sce.trustAsHtml($scope.newsletterBuy);
                $scope.subscribeType = "estateBuy";
                $scope.formType = "Real Estate - For Sale";
            } else if (type === "rent") {
                $scope.newsletter = $sce.trustAsHtml($scope.newsletterRent);
                $scope.subscribeType = "estateRent";
                $scope.formType = "Real Estate - Rental";
            } else if (type === "for-sale") {
                $scope.newsletter = $sce.trustAsHtml($scope.newsletterComSale);
                $scope.subscribeType = "commercialBuy";
                $scope.formType = "Commercial - For Sale";
            } else if (type === "for-lease") {
                $scope.newsletter = $sce.trustAsHtml($scope.newsletterComLease);
                $scope.subscribeType = "commercialLease";
                $scope.formType = "Commercial - For Lease";
            }

        }

        $scope.logout = function () {
            $scope.myPromiseAgency = $http.get("/agencies/logoutAgency/")
                .success(function (result) {
                    $location.url("/login");
                });
        }
        $scope.host = window.location.host;
        onload();

    });
