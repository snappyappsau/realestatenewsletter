'use strict';

/**
 * @ngdoc function
 * @name wwwApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the wwwApp
 */
angular.module('wwwApp')
    .controller('MainCtrl', function ($scope, $http, notificationService, $location) {
        $scope.loadAgencyDetails = function (agencyId) {
            if (agencyId.length == 6) {
                $scope.myPromise = $http.get("/agencies/agencydetails/" + agencyId)
                    .success(function (result) {
                        if (result.status === true) {
                            $scope.agency = result;
                            $scope.$watch('$scope.agency', function () {
                                $('html, body').animate({
                                    scrollTop: $("#stp2").offset().top
                                }, 500);
                            });
                        } else {
                            notificationService.error('No agent could be found for the id you entered. Please ensure you entered your agent id correctly.');
                            $scope.agency = null;
                        }
                    })
            }
        }

        $scope.select = function (agencyId) {
            $scope.selectedAgency = $scope.agency;
            $scope.selectedAgency.agencyId = agencyId;
            $scope.$watch('isDisplayed', function () {
                $('html, body').animate({
                    scrollTop: $("#stp3").offset().top
                }, 500);
            });
        }

        $scope.register = function () {
            $scope.myPromise = $http.post("/agencies/create", $scope.selectedAgency)
                .success(function (result) {
                    if (result.success === true) {
                        $location.url("/login");
                    } else {
                        notificationService.error(result.err);
                    }

                })
        }
    });
