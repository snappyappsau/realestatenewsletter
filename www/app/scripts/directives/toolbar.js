'use strict';

/**
 * @ngdoc directive
 * @name wwwApp.directive:toolbar
 * @description
 * # toolbar
 */
angular.module('wwwApp')
    .directive('toolbar', function () {
        return {
            templateUrl: '../../views/toolbar.html',
            restrict: 'E',
            scope: {},
            controller: function ($scope,$location) {
                $scope.isActive = function (myClass) {
                    if ($location.url() === myClass) {
                        return true
                    } else {
                        return false
                    }
                }

            }
        };
    });
