/**
 * UpdateController
 *
 * @description :: Server-side logic for updating listings from Real Estate Agents
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var request = require('request'),
    cheerio = require('cheerio'),
    async = require('async'),
    format = require('util').format;

module.exports = {

    test: function (req, res) {
        var reddits = ['funny', 'pics', 'node']
            , concurrency = 2;

        var data = [];
        async.eachLimit(reddits, concurrency, function (reddit, next) {
            var url = format('http://reddit.com/r/%s', reddit);
            request(url, function (err, response, body) {
                if (err) {
                    next(err);
                    return;
                }

                var $ = cheerio.load(body);

                $('a.title').each(function () {
                    data.push({ title: $(this).text(), url: $(this).attr('href') });
                });

                next();
            });
        }, function (err) {
            if (err) {
                res.json(err);
            } else {
                res.json(data);
            }
        });
    },

    listings: function (req, res) {
        var agent = req.params.id;
        var type = req.params.type;
        var properties = req.params.properties;

        //Get property type
        var url = '';
        if (type === "commercial") {
            url = 'http://www.realcommercial.com.au/' + properties + '/by-' + agent + '/list-1?activeSort=list-date';
            request(url, function (err, response, body) {
                var data = processPageCommercial(body, agent);
                if (data.listings.length === 0) {
                    res.json("fail");
                } else {
                    res.view('email', { data: data });
                }

            });
        } else {
            url = 'http://www.realestate.com.au/' + properties + '/by-' + agent + '/list-1?activeSort=list-date';
            request(url, function (err, response, body) {
                var data = processPageEstate(body, agent);
                if (data.listings.length === 0) {
                    res.json("fail");
                } else {
                    res.view('email', { data: data });
                }

            });
        }

        //Get agent properties       

    },
};

function processPageEstate(body, agent) {
    var $ = cheerio.load(body);

    var agency = {
        logo: $('img.logo').attr('src'),
        name: $('.contactDetails h2').text(),
        address: $('.street-address').text(),
        description: $('div.folderAgentInfo .description').html(),
        phone: $('.phone a').attr('data-value'),
        website: $('.web a').attr('href'),
        twitter: $('.twitter-url a').attr('href'),
        facebook: $('.facebook-url a').attr('href'),
        listings: [],
    };

    $('.resultBody').each(function () {
        var item = {
            address: $(this).find('.vcard a').text(),
            url: 'http://www.realestate.com.au' + $(this).find('.vcard a').attr('href'),
            type: $(this).find('.propertyType').text(),
            bedrooms: $(this).find('.linkList li').eq(0).text(),
            bathrooms: $(this).find('.linkList li').eq(1).text(),
            carports: $(this).find('.linkList li').eq(2).text(),
            title: $(this).find('.title').text(),
            description: $(this).find('.description').text(),
            price: $(this).find('.priceText').text(),
            img: $(this).find('.frame0 img').attr('src') || $(this).find('.standardFrame img').attr('src'),
            agent: agent
        };
        agency.listings.push(item);
    });
    return agency;
}

function processPageCommercial(body, agent) {
    var $ = cheerio.load(body);

    var agency = {
        logo: $('img.logo').attr('src'),
        name: $('.contactDetails h2').text(),
        address: $('.street-address').text(),
        description: $('div.folderAgentInfo .description').html(),
        phone: $('.phone a').attr('data-value'),
        website: $('.web a').attr('href'),
        twitter: $('.twitter-url a').attr('href'),
        facebook: $('.facebook-url a').attr('href'),
        listings: [],
    };

    $('.resultBody').each(function () {
        var item = {
            address: $(this).find('.ellipsis a').text(),
            url: 'http://www.realcommercial.com.au' + $(this).find('.ellipsis  a').attr('href'),
            type: $(this).find('.propType').text(),         
            title: $(this).find('.title').text(),
            description: $(this).find('.description').text(),
            price: $(this).find('.priceText').text(),
            img: $(this).find('.frame0 img').attr('src') || $(this).find('.photoviewer img').attr('src'),
            agent: agent
        };
        agency.listings.push(item);
    });
    return agency;
}