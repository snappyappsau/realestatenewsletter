/**
 * AgenciesController
 *
 * @description :: Server-side logic for managing agencies
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var q = require('q'),
    request = require('request'),
    cheerio = require('cheerio');

var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill(process.env.MANDRILL_APIKEY || "qeZ923oy5LM7aXONubVR8Q");

module.exports = {
    send: function (req, res) {
        var agencyId = req.params.id || "qzdhzg";
        Agencies.native(function (err, agencies) {
            agencies.find({ agencyId: agencyId })
                .toArray(function (err, docs) {
                    if (docs.length) {
                        loadNewsletter(agencyId)
                            .then(function (html) {
                                docs[0].subscribers.forEach(function (subscriber) {
                                    sendNewsletter(subscriber, docs[0], html);
                                    res.json({ success: true, message: "Success sending emails. Check logs for further details" })
                                });
                            }, function (err) {
                                res.json(err);
                            })
                    } else {
                        res.json({ success: false, message: "Agency not found", data: docs, err: err });
                    }
                })
        })
    },
    create: function (req, res) {
        var agencyId = req.body.agencyId;
        Agencies.findOne({ agencyId: agencyId }).
            exec(function (err, docs) {
                if (docs) {
                    res.json({ success: false, docs: docs, err: "This agent has already been registered." });
                } else {
                    scrapeAgencyData(agencyId)
                        .then(function (agency) {
                            Agencies.native(function (err, agencies) {
                                agency.agencyId = agencyId;
                                agency.subscribers = [];
                                agency.email = req.body.email;
                                agency.password = req.body.password;
                                agencies.insert(agency, function (err, doc) {
                                    if (err) {
                                        res.json({ success: false, err: err });
                                    } else {
                                        res.json({ success: true, data: docs });
                                    }
                                })
                            })
                        })
                }
            })
    },
    empty: function (req, res) {
        Agencies.native(function (err, agencies) {
            agencies.remove({}, function (err, docs) {
                if (err) {
                    res.json(err);
                } else {
                    res.json(docs);
                }
            })
        });
    },
    loginAgency: function (req, res) {
        if (req.session.agency) {
            req.session.auth = true;
            res.json({ success: true });
        } else {
            res.json({ success: false });
        }
    },
    logoutAgency: function (req, res) {
        if (req.session.agency) {
            req.session.agency = null;
        }
        req.session.auth = false;
        res.json({ success: true });

    },
    getAuthAgency: function (req, res) {
        if (req.session.auth === true) {
            res.json({ success: true, agency: req.session.agency });
        } else {
            res.json({ success: false, data: "No user found" });
        }
    },
    getAgency: function (req, res) {
        var username = req.params.id;
        Agencies.findOne({ email: username }).exec(function (err, docs) {
            if (docs) {
                req.session.agency = docs;
                res.json({ success: true, data: docs, err: err });
            } else {
                res.json({ success: false, message: "Agency not found", data: docs, err: err });
            }

        })
    },
    addSubscriber: function (req, res) {
        if (req.body) {
            var user = req.body.user;
            Agencies.find({ agencyId: req.body.agencyId })
                .exec(function (err, docs) {
                    docs[0].subscribers.push(user);
                    Agencies.update({ id: docs[0].id }, docs[0])
                        .exec(function (err, docs) {
                            res.view('success');
                        })
                })
        } else {
            res.view('fail');
        }
    },
    unsubscribe: function (req, res) {
        try {
            var agencyId = req.query.id;
            var email = req.query.email;
            var found;
            console.log(agencyId, email)
            Agencies.find({ agencyId: agencyId })
                .exec(function (err, docs) {
                    console.log(err, docs)
                    docs.forEach(function (agency) {
                        agency.subscribers.forEach(function (subscriber) {
                            console.log(subscriber)
                            if (subscriber.email == email) {
                                var index = agency.subscribers.indexOf(subscriber);
                                agency.subscribers.splice(index, 1);
                                console.log("index", index);
                                console.log(agency.subscribers);
                                Agencies.update({ agencyId: agencyId }, agency).exec(function (err, docs) {
                                    console.log(err, docs);
                                    found = true;
                                })
                            }
                            res.json({ success: found });

                        });
                    })
                    if (!docs.length) {
                        res.json({ success: false, message: "no user found" });
                    }
                })
        } catch (e) {
            console.log(e);
            res.json(e);
        }
    },
    agencyDetails: function (req, res) {
        scrapeAgencyData(req.params.id)
            .then(function (result) {
                res.json(result);
            })
    },
    getNewsletter: function (req, res) {
        loadNewsletter(req.params.id, req.headers.host, req.params.type, req.params.properties)
            .then(function (html) {
                res.json({ data: html });
            }, function (err) {
                res.json({ message: "There was an error loading the newsletter", data: err });
            })
    }
}


function sendNewsletter(subscriber, agency, html) {
    var message = {
        "html": html,
        "subject": "Property Update from " + agency.name,
        "from_email": agency.email || "ryan.knell@gmail.com",
        "from_name": agency.name || "Ryan Knell",
        "to": [
            {
                "email": subscriber.email,
                "name": subscriber.name,
                "type": "to"
            }
        ],
        "auto_text": true
    };
    mandrill_client.messages.send({ "message": message, async: true }, function (result) {
        console.log(result);
    })
}

function scrapeAgencyData(agencyId) {
    var deferred = q.defer();
    var url = 'http://www.realestate.com.au/buy/by-' + agencyId + '/list-1?activeSort=list-date';
    request(url, function (err, response, body) {
        var $ = cheerio.load(body);

        var agency = {
            status: true,
            logo: $('img.logo').attr('src'),
            name: $('.contactDetails h2').text(),
            address: $('.street-address').text(),
            photo: $('.photo img').attr('src'),
            description: $('div.folderAgentInfo .description').text(),
            phone: $('.phone a').attr('data-value'),
            website: $('.web a').attr('href'),
            twitter: $('.twitter-url a').attr('href'),
            facebook: $('.facebook-url a').attr('href'),
            listings: []
        };

        if ($('.error p').text() === "Listing cannot be found. Please try again later.") {
            agency.status = false;
        }
        deferred.resolve(agency);
    });
    return deferred.promise;
}

function loadNewsletter(agencyId, host, type, properties) {
    var deferred = q.defer();
    request("http://" + host + "/update/listings/" + agencyId + "/" + type + "/" + properties, function (err, response, html) {
        if (err) {
            deferred.reject(err);
        } else {
            if (html) {
                deferred.resolve(html);
            } else {
                deferred.reject({ success: false, message: "No HTML loaded", data: html });
            }
        }
    });

    return deferred.promise;
}