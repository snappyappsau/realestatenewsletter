var express = require("express"),
    path = require("path")

module.exports.express = {
    customMiddleware: function (app) {
        console.log("Executing custom middleware");
        app.use("/", express.static(path.join(__dirname, "..", "www", "app")));
    }
};